class Course {
    #price
    get price() {
      return "$" + this.#price
    }
  
    set price(value) {
      if(value < 0) {
        throw "Invalid value"
      }
      this.#price = value
    }
  
    constructor(cTitle, cLength, cPrice){
      this.title = cTitle
      this.length = cLength
      this.price = cPrice
    }
  
    calculateValue(){
      return this.length/this.#price
    }
  
    printSummary(){
      console.log("Title: " + this.title + " Lenth: " + this.length +  " Price: " + this.price) 
    }
  }
  
  const htmlCourse = new Course("HTML Course", 10, 50)
  const cssCourse = new Course("CSS", 30, 70)
  //htmlCourse.#price = -100
  console.log(htmlCourse)
  console.log(cssCourse)
  
  
  console.log(htmlCourse.calculateValue())
  console.log(cssCourse.calculateValue())
  
  htmlCourse.printSummary()
  cssCourse.printSummary()
  
  class PracticalCourse extends Course {
    constructor(pcTitle, pcLength, pcPrice, pcNumOfExc){
      super(pcTitle, pcLength, pcPrice)
      this.numOfExercises = pcNumOfExc
      this.length = pcLength
      this.pcPrice = pcPrice
      this.pcLength = pcLength
    }
  
    calculateValue(){
        return ( this.pcLength/this.pcPrice ) * 10
    }
  }
  
  const jsCourse  = new PracticalCourse("JavaScript Course", 15, 80, 5)
  
  console.log(jsCourse);
  console.log(jsCourse.calculateValue());
  jsCourse.printSummary()
  
  class TheoreticalCourse extends Course {
    publish(){
      console.log("Publishing")
    }
  }
  
  console.log("----------------------------------");
  
  const reactCourse = new TheoreticalCourse("React Course", 20, 40)
  console.log(reactCourse);
  console.log(reactCourse.calculateValue());
  reactCourse.printSummary()
  reactCourse.publish()
  