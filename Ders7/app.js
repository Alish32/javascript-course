
const keyName = 'alish najafzade'

const person = {
    name: 'Alish',
    age: 26,
    hobbies: ['sports', 'walking'],
    greet() {
        alert('salam')
    },
    '5': 'Hello',
    [keyName]: 'QWERTYU',
    getUpperCaseName(){
        console.log(this)
        // alert('asdfghj')
        return this.name.toUpperCase()
    }
}
let {getUpperCaseName} = person

// getUpperCaseName = getUpperCaseName.bind(person)

console.log(getUpperCaseName.call(person, []));
console.log(getUpperCaseName.apply(person, a, b,c ));