const addButton = document.getElementById('add')
const getButton = document.getElementById('get')
const userName = document.getElementById('user')

addButton.addEventListener('click', () => {
    const userData = {
        title: userName.value
    }

    axios.post('http://localhost:3000/add-user', userData).then(response => {
        console.log(JSON.stringify(response.data))
    }).catch(err => {
        console.log(JSON.stringify(err))
    })
})

getButton.addEventListener('click', () => {
    axios.get('http://localhost:3000/user').then(response => {
        
        response.data.users.forEach(element => {
            alert(element.title)
        });
    })
})
