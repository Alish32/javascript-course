let currentResult = 0
let calculationDescription = ''

const logEntries = []

function getUserNumberInput() {
    return +userInput.value
}

function writeToLog(operatorIdentifier, prevResult, operationNumber, newResult) {
    const logEntry = {
        operation: operatorIdentifier,
        prevResult: prevResult,
        number: operationNumber,
        result: newResult
    }

    logEntries.push(logEntry)
    console.log(logEntries)
}

function add() {
    enteredNumber =  getUserNumberInput()
    const initialresult = currentResult
    console.log('Input' + enteredNumber, currentResult)
    const calcDescription = `${currentResult} + ${enteredNumber}`
    currentResult += enteredNumber
    outputResult(currentResult, calcDescription)
    writeToLog("ADD", initialresult, enteredNumber, currentResult)
}

function subtract() {
    enteredNumber =  getUserNumberInput()
    const calcDescription = `${currentResult} - ${enteredNumber}`
    currentResult -= enteredNumber
    outputResult(currentResult, calcDescription)
}

function multiply() {
    enteredNumber =  getUserNumberInput()
    const calcDescription = `${currentResult} * ${enteredNumber}`
    currentResult *= enteredNumber
    outputResult(currentResult, calcDescription)
}

function divide() {
    enteredNumber =  getUserNumberInput()
    const calcDescription = `${currentResult} / ${enteredNumber}`
    currentResult /= enteredNumber
    outputResult(currentResult, calcDescription)
}

addBtn.addEventListener('click', add)
subtractBtn.addEventListener('click', subtract)
multiplyBtn.addEventListener('click', multiply)
divideBtn.addEventListener('click', divide)